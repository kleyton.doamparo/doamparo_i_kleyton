package cat.itb.formulariowapo.Views;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;

import cat.itb.formulariowapo.retrofit.UserApi;
import cat.itb.formulariowapo.retrofit.UserApiProvider;
import cat.itb.formulariowapo.retrofit.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    UserApiProvider userApiProvider = new UserApiProvider();
    UserApi userApi = userApiProvider.getUserApi();
    MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    MutableLiveData<String> error = new MutableLiveData<String>();


    public String datePicker(Long aLong) {
        Date date = new Date(aLong);
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        String dataText = format1.format(date);
        return dataText;
    }

    public void login(String username) {
        loading.setValue(true);
        Call<UserSession> call = userApi.login(username);
        call.enqueue(new Callback<UserSession>() {
            @Override
            public void onResponse(Call<UserSession> call, Response<UserSession> response) {
                if (response.body().isLogged()) {
                    logged.setValue(true);
                } else {
                    error.setValue(response.body().getError());
                }
                loading.setValue(false);
            }

            @Override
            public void onFailure(Call<UserSession> call, Throwable t) {
                error.setValue(t.getLocalizedMessage());
                loading.setValue(false);
            }
        });
    }

    public void register(String username){
        loading.setValue(true);
        Call<UserSession> call = userApi.register(username);
        call.enqueue(new Callback<UserSession>() {
            @Override
            public void onResponse(Call<UserSession> call, Response<UserSession> response) {
                if (response.body().isLogged()) {
                    logged.setValue(true);
                }else {
                    error.setValue(response.body().getError());
                };
            }

            @Override
            public void onFailure(Call<UserSession> call, Throwable t) {

            }
        });

    }
}
