package cat.itb.formulariowapo.Views;

import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;

import cat.itb.formulariowapo.R;

public class RegisterView extends Fragment {

    TextInputLayout registerUserName;
    TextInputLayout registerPassword;
    TextInputLayout registerRepeatPassword;
    TextInputLayout birthDATE;
    TextInputLayout gender;
    TextInputLayout email;

    private MainViewModel mViewModel;

    public static RegisterView newInstance() {
        return new RegisterView();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_view_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerUserName = view.findViewById(R.id.registerUsername);
        registerPassword = view.findViewById(R.id.registerNewPassword);
        registerRepeatPassword = view.findViewById(R.id.registerRepeatNewPassword);
        birthDATE=view.findViewById(R.id.birthdate);
        gender=view.findViewById(R.id.gender);
        email=view.findViewById(R.id.email);

        birthDATE.setOnClickListener(this::abrirCalendario);
        view.findViewById(R.id.btn_registerEnviarForm).setOnClickListener(this::enviar);
        view.findViewById(R.id.btn_registerLogin).setOnClickListener(this::volverALogin);
        gender.getEditText().setOnClickListener(this::seleccio);


    }

    private void volverALogin(View view) {
        Navigation.findNavController(view).navigate(R.id.action_registerView2_to_loginView2);
    }

    private void seleccio(View view) {
        String[] arrayOptions = {"Female","Male"};
        new MaterialAlertDialogBuilder(getContext())
                .setTitle(R.string.gender)
                .setItems(arrayOptions, new DialogInterface.OnClickListener (){
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        gender.getEditText().setText(arrayOptions[i]);
                    }
                })
                .show();
    }

    private void abrirCalendario(View view) {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText(R.string.birthdate);
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());


    }

    private void doOnDateSelected(Long aLong) {
        Date data = new Date(aLong);
        birthDATE.getEditText().setText(data.toString());
    }

    private void enviar(View view) {
        if(validate()){
            //hacer algo, cambiar de layout?
            Navigation.findNavController(view).navigate(R.id.action_registerView2_to_loggedView);
        }
    }

    private boolean validate() {
        registerUserName.setError("");
        registerPassword.setError("");
        registerRepeatPassword.setError("");
        boolean valid = true;
        valid = validateNotEmpty(registerUserName,valid);
        valid = validateNotEmpty(registerPassword,valid);
        valid = validateNotEmpty(registerRepeatPassword,valid);
        valid = validatePassword()&&valid;
        valid = validateEmail()&&valid;

        return valid;

    }

    private boolean validatePassword() {
        boolean valid=false;
        if(registerRepeatPassword.getEditText().getText().toString().matches(registerPassword.getEditText().getText().toString())){
            valid=true;
            //Toast.makeText(getContext(),"Está bien escrito!",Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    private boolean validateEmail() {
        boolean valid=false;
        if(Patterns.EMAIL_ADDRESS.matcher(email.getEditText().getText().toString().trim()).matches()){
            valid=true;
            //Toast.makeText(getContext(),"Está bien escrito!",Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

    private boolean validateNotEmpty(TextInputLayout input,boolean valid){
        if(input.getEditText().getText().toString().isEmpty()){
            input.setError("Este campo no puede estar vacio");
            if (valid){
                //scroll
                scrollTo(input);
            }
            return false;
        }
        return valid;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView,targetView);
    }

}
