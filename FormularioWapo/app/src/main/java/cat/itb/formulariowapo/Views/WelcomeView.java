package cat.itb.formulariowapo.Views;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.formulariowapo.R;

public class WelcomeView extends Fragment {

    private MainViewModel mViewModel;

    public static WelcomeView newInstance() {
        return new WelcomeView();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_view_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btn_welcomeLogin).setOnClickListener(this::GoToLogin);
        view.findViewById(R.id.btn_welcomeRegister).setOnClickListener(this::GoToRegister);
    }

    private void GoToRegister(View view) {
        Navigation.findNavController(view).navigate(R.id.action_welcomeView_to_registerView2);
    }

    private void GoToLogin(View view) {
        Navigation.findNavController(view).navigate(R.id.action_welcomeView_to_loginView2);
    }
}
