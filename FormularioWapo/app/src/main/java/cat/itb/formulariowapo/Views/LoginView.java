package cat.itb.formulariowapo.Views;

import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.textfield.TextInputLayout;

import cat.itb.formulariowapo.R;

public class LoginView extends Fragment {

    TextInputLayout username;
    TextInputLayout password;
    private ProgressDialog dialog;
    private MainViewModel mViewModel;

    public static LoginView newInstance() {
        return new LoginView();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_view_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btn_loginLogin).setOnClickListener(this::GoToLogged);
        view.findViewById(R.id.btn_loginRegister).setOnClickListener(this::GoToRegister);
        view.findViewById(R.id.btn_loginForgotPassword).setOnClickListener(this::GoToGetPassword);

        dialog = new ProgressDialog(getActivity());

        mViewModel.loading.observe(this,this::loading);
        mViewModel.logged.observe(this,this::logged);

    }

    private void comprobarFormulari(View view) {
        if (validate()){
            mViewModel.login(username.getEditText().getText().toString());
        }
    }

    private void loading(Boolean aBoolean) {
        if (aBoolean){
            dialog = ProgressDialog.show(getContext(),"Loading","Loading",true);

        }else {
            dialog.dismiss();
        }
    }

    private boolean validate() {
        boolean valid = true;
        if(username.getEditText().getText().toString().isEmpty() || password.getEditText().getText().toString().isEmpty()){
            valid = false;
        }
        return valid;
    }

    private void logged(Boolean aBoolean) {
        if (aBoolean){
            Navigation.findNavController(getView()).navigate(R.id.action_loginView2_to_loggedView);
        }
    }

    private boolean validateNotEmpty(TextInputLayout input) {
        if (input.getEditText().getText().toString().isEmpty()) {
            input.setError("Introdueix camp obligatori **");
            scrollTo(input);
            return false;
        }
        return true;
    }

    private void scrollTo(TextInputLayout inputEditText){
        inputEditText.getParent().requestChildFocus(inputEditText,inputEditText);
    }


    private void GoToGetPassword(View view) {
        //Recuperar contraseña
    }

    private void GoToRegister(View view) {
        Navigation.findNavController(view).navigate(R.id.action_loginView2_to_registerView2);
    }



}
